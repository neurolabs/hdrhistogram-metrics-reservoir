package org.mpierce.metrics.reservoir.hdrhistogram;

import com.codahale.metrics.Reservoir;
import com.codahale.metrics.Snapshot;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class HdrHistogramResetOnSnapshotCountingReservoirTest extends HdrHistogramReservoirTestCase {

    @Override
    Reservoir getReservoir() {
        return new HdrHistogramResetOnSnapshotCountingReservoir();
    }

    @Test
    public void testResetsOnSnapshot() {
        r.update(1);
        r.update(2);
        r.update(3);

        Snapshot snapshot = r.getSnapshot();

        assertEquals(3, snapshot.size());

        assertEquals(0, r.getSnapshot().size());
    }

    @Test
    public void testMinAfterReset() {
        r.update(1);
        r.update(2);
        r.update(3);

        Snapshot snapshot = r.getSnapshot();

        assertEquals(1, snapshot.getMin());
    }

    @Test
    public void testSizeCanBeCalledRepeatedly() {
        r.update(1);
        r.update(2);
        r.update(3);

        assertEquals(3, r.size());
        assertEquals(3, r.size());

        Snapshot snapshot = r.getSnapshot();

        assertEquals(0, r.size());
    }
}
