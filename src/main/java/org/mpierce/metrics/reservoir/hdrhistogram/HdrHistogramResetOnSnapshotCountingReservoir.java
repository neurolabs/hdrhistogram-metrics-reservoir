package org.mpierce.metrics.reservoir.hdrhistogram;

import com.codahale.metrics.Snapshot;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A Reservoir that, in addition to resetting its internal state every time a snapshot is taken
 * (see {@link HdrHistogramResetOnSnapshotReservoir}), counts the number of values recorded for a given interval, with
 * the goal of implementing a size method that can be called multiple times, without resetting state as a side effect.
 */
@ThreadSafe
public final class HdrHistogramResetOnSnapshotCountingReservoir extends HdrHistogramResetOnSnapshotReservoir {
  @Nonnull
  private final AtomicInteger count = new AtomicInteger(0);

  @Override
  public synchronized void update(long value) {
    count.incrementAndGet();
    super.update(value);
  }

  @Override
  public int size() {
    return count.get();
  }

  @Override
  public synchronized Snapshot getSnapshot() {
    count.set(0);
    return super.getSnapshot();
  }
}
